const div = document.getElementById('country-div')



fetch(`https://restcountries.com/v3.1/all`)
.then(res => res.json())
.then(data => {
    data.forEach(country =>{
        const article = document.createElement('article')
        const countryName = document.createElement('h2')
        countryName.textContent = country.name.common;

    const flag = document.createElement('img')
    flag.src = country.flags.png
    flag.alt = `Flag of ${country.name.common}`

    article.appendChild(flag)
    article.appendChild(countryName)

    div.appendChild(article)


    });
})
.catch(error => console.error('Error fetching countries:',error))

